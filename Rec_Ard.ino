#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <String.h>

#define LED_Joystik_Stanga_B      7
#define LED_Joystik_Dreapta_B     8
#define LED_Buton_Stanga          9
#define LED_Buton_Dreapta         10
#define LED_Buton_Sus             11
#define LED_Buton_Jos             12
#define LED_Buton_Mijloc          13

//char sir_rec[7] = {'0','0','0','0','0','0','0'};
char sir_rec[100];
char buffer_ecran[80];

char   rez_Butoane[10];
char   rez_JS[13];
char   rez_JD[13];
char   *tmpStr;

// Afisajul LCD la adresa 0x27 pentru 20 caractere si 4 linii
LiquidCrystal_I2C lcd(0x20, 20, 4);
void  afis_ecran(char sir_afis[80]);
int  cmd_led(char buton);

void setup() {
  char tmpRec;

  pinMode(LED_Joystik_Stanga_B,OUTPUT);
  pinMode(LED_Joystik_Dreapta_B,OUTPUT);
  pinMode(LED_Buton_Stanga,OUTPUT);
  pinMode(LED_Buton_Dreapta,OUTPUT);
  pinMode(LED_Buton_Sus,OUTPUT);
  pinMode(LED_Buton_Jos,OUTPUT);
  pinMode(LED_Buton_Mijloc,OUTPUT);

  digitalWrite(LED_Joystik_Stanga_B,LOW);
  digitalWrite(LED_Joystik_Dreapta_B,LOW);
  digitalWrite(LED_Buton_Stanga,LOW);
  digitalWrite(LED_Buton_Dreapta,LOW);
  digitalWrite(LED_Buton_Sus,LOW);
  digitalWrite(LED_Buton_Jos,LOW);
  digitalWrite(LED_Buton_Mijloc,LOW);

 // initialize the LCD
	lcd.begin();
//            *       Linia 0    **     Linia 1      **   Linia 2        **       Linia 3    *  
//            01234567890123456789012345678901234567890123456789012345678901234567890123456789
  afis_ecran(" PROIECT DE DIPLOMA   ZAMFIRESCU FLAVIA   TELECOMANDA RADIO  RECEPTIE ARDUINO   ");
  delay(100);
  lcd.clear();
  Serial.begin(4800);
  while(1){
    Serial.write('S');
    if (Serial.available() > 0){
        tmpRec = Serial.read();
        if(tmpRec == 'R'){
//                     *       Linia 0    **     Linia 1      **   Linia 2        **       Linia 3    *  
//                      01234567890123456789012345678901234567890123456789012345678901234567890123456789
            afis_ecran(" Conexiune Stabila                                                              ");
            delay(100);
            lcd.clear();
            break;
        }
    }    
  }      
}

void loop() { 

  Serial.write('D');
  if (Serial.available() > 0) {
      Serial.readBytes(sir_rec,50);
      tmpStr = strtok(sir_rec, ";");
      strcpy(rez_JS,tmpStr);
      strcat(rez_JS,"\0");
      lcd.setCursor(1,0);
      lcd.print(rez_JS);
      lcd.print("      ");
      tmpStr = strtok(NULL, ";");
      strcpy(rez_JD,tmpStr);
      strcat(rez_JD,"\0");
      lcd.setCursor(1,1);
      lcd.print(rez_JD); 
      lcd.print("      ");
      tmpStr = strtok(NULL, ";");
      strcpy(rez_Butoane,tmpStr);
      strcat(rez_Butoane,"\0");      
      lcd.setCursor(1,2);
      lcd.print(rez_Butoane);            
      lcd.print("  ");
//comenzi LED-uri
      digitalWrite(LED_Buton_Stanga,cmd_led(rez_Butoane[0]));
      digitalWrite(LED_Buton_Dreapta,cmd_led(rez_Butoane[2]));            
      digitalWrite(LED_Buton_Sus,cmd_led(rez_Butoane[6]));  
      digitalWrite(LED_Buton_Jos,cmd_led(rez_Butoane[4]));
      digitalWrite(LED_Buton_Mijloc,cmd_led(rez_Butoane[8]));
      digitalWrite(LED_Joystik_Stanga_B,cmd_led(rez_JS[0]));      
      digitalWrite(LED_Joystik_Dreapta_B,cmd_led(rez_JD[0]));  
  }
}

//afisare sir pe LCD
void  afis_ecran(char sir_afis[80]){
int   col;
int   linie;
int   i;

  i=0;
  for(linie=0;linie<4;linie++){
    for(col=0;col<20;col++){
      lcd.setCursor(col,linie);
      lcd.print(sir_afis[i]);
      i++;
    }
  }
}

int  cmd_led(char buton){
  if(buton == '0')
    return(LOW);
  if(buton == '1')
    return(HIGH);  
}