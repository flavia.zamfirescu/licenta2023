#include <REG51F.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define FALSE 0
#define TRUE  1

#define BAUD_LSB 0xF3
#define BAUD_MSB 0xF3

/*--- HW definitii pentru sistem ---*/
sbit D_RS  = P3^4;
sbit D_E   = P3^5;

sbit LED_JS_B      = P1^0;
sbit LED_JD_B      = P1^1;
sbit LED_B_Stanga  = P1^2;
sbit LED_B_Dreapta = P1^3;
sbit LED_B_Jos     = P1^4;
sbit LED_B_Sus     = P1^5;
sbit LED_B_Mijloc  = P1^6;

char buffer_rec[80];
char Buton_JS[2];
char Buton_JD[2];
char Buton_Stanga[2];
char Buton_Dreapta[2];
char Buton_Jos[2];
char Buton_Sus[2];
char Buton_Mijloc[2];
char JS_X[5];
char JS_Y[5];
char JD_X[5];
char JD_Y[5];
char *tmpSir;
const char *delimitator =",";
//functia DELAY
void delay(int const_timp);

//functia init_micro
void init_micro(void);

//functia pentru transmia unei comenzi catre DISPLAY
void Comanda_Display(unsigned char Data);

//functia pentru afisarea unei date pe DISPLAY
void Data_Display(unsigned char Data);

//functia pentru initializarea Display-ului
void init_display(void);
//functia afisare buffer
void afisare_buffer(char text[80]);
void rec_date(void);
bit LED(char cmd[]);
void sparge_sir(void);

void main(void){
	//initializare microcontroler
	init_micro();
//initializare display
	init_display();
	delay(100);
//	              01234567890123456789012345678901234567890123456789012345678901234567890123456789  
	afisare_buffer(" PROIECT DE DIPLOMA   ZAMFIRESCU FLAVIA   TELECOMANDA RADIO     RECEPTIE 8051   ");
//stabilire conexiune
	while(1){
		SBUF = 'S';	
		while(TI == 0);
		TI = 0;
		if(RI==1)
			break;
	}	
//	while(RI == 0);
	RI = 0;
//Display clear
	Comanda_Display(0x01);
	delay(100);
	while(1){
		rec_date();
//"spargere" sir receptie
//		sparge_sir();
		LED_JS_B      = LED(Buton_JS);
		LED_JD_B      = LED(Buton_JD);
		LED_B_Stanga  = LED(Buton_Stanga);
		LED_B_Dreapta = LED(Buton_Dreapta);
		LED_B_Jos     = LED(Buton_Jos);
		LED_B_Sus     = LED(Buton_Sus);
		LED_B_Mijloc  = LED(Buton_Mijloc);
	}	
}

/***************************************************************************
****************************************************************************
**                                                                        **
**      DEFINITII FUNCTII                                                 **
**                                                                        **
****************************************************************************
****************************************************************************/

/***************************************************************************
**                                                                        **
**    functia DELAY	                                                      **
**                                                                        **
***************************************************************************/
void delay(int const_timp){
int i,j;

	for(i=0;i<const_timp;i++){
		for(j=0;j<100;j++);
	}
}

/***************************************************************************
**                                                                        **
**    functia init_micro pentru initializarea microcontrolerului          **
**                                                                        **
***************************************************************************/
void init_micro(void){
//anuleaza comenzi
	D_E = FALSE;
	D_RS = FALSE;

	P1 = 0x00;
//Timer 1 mod de lucru autoload
	TMOD = 0x20;

//constanta de timp seriala
	TH1 = BAUD_MSB;
	TL1 = BAUD_LSB;

//programare seriala
	SM0  = 0;
	SM1  = 1;
	PCON = 0x80;

//validare receptie
	REN = 1;
	TR1 = 1;
}


/***************************************************************************
**                                                                        **
**    functia Comanda_Display pentru incarcarea unei comenzi la display   **
**                                                                        **
***************************************************************************/
void Comanda_Display(unsigned char Data){
	
//incarca data in registru
	P0 = Data;
//generare comanda RS = 0
	D_RS = FALSE;
	delay(1);
//generare E
	D_E = TRUE;
	delay(1);
//anuleaza comenzi
	D_E = FALSE;
	D_RS = FALSE;
}


/***************************************************************************
**                                                                        **
**    functia Data_Display pentru afisarea unei date pe display           **
 ***************************************************************************/
void Data_Display(unsigned char Data){
//incarca data in registru
	P0 = Data;
//generare comanda RS = 1
	D_RS = TRUE;
	delay(1);
//generare E
	D_E = TRUE;
	delay(1);
//anuleaza comenzi
	D_E = FALSE;
	D_RS = FALSE;
}


/***************************************************************************
**                                                                        **
**    functia pentru initializare  display                                **
 ***************************************************************************/
void init_display(void){
//asteapta 30 msec
	delay(150);
//setare functii
	Comanda_Display(0x3C); 
//asteapta 1 msec
	delay(1);
//Display ON/OFF
	Comanda_Display(0x0C);	//0E
//asteapta 1 msec
	delay(1);
//Display clear
	Comanda_Display(0x01);
//asteapta 1,53 msec
	delay(10);
//Entry mode
	Comanda_Display(0x06);
	delay(10);
}

/***************************************************************************
**                                                                        **
**    functia pentru afisarea unui ecran complet la display               **
 ***************************************************************************/
void afisare_buffer(char text[80]){
unsigned char poz_display;
int i;


//linia 1
	i=0;
	
	for(poz_display = 0x80; poz_display < 0x94; poz_display++){
		Comanda_Display(poz_display);
		Data_Display(text[i]);
		i++;
	}

//linia 2
	for(poz_display = 0xC0; poz_display < 0xD4; poz_display++){
		Comanda_Display(poz_display);
		Data_Display(text[i]);
		i++;
	}

//linia 3
	for(poz_display = 0x94; poz_display < 0xA8; poz_display++){
		Comanda_Display(poz_display);
		Data_Display(text[i]);
		i++;
	}

//linia 4
	for(poz_display = 0xD4; poz_display < 0xE8; poz_display++){
		Comanda_Display(poz_display);
		Data_Display(text[i]);
		i++;
	}
}

/***************************************************************************
**                                                                        **
**    functia pentru receptia datelor                                     **
 ***************************************************************************/
void rec_date(void){
	int tmpCnt;

	tmpCnt = 0;
	RI=0;
	while(1){
		SBUF = 'D';
		while(TI == 0);
		TI=0;
		if(RI == 1)
			break;
	}	
  while(1){
				if(RI == 1){
					buffer_rec[tmpCnt] = SBUF;		
					RI = 0;
					if(buffer_rec[tmpCnt] == '\0'){ 
//						buffer_rec[tmpCnt] = '\0';
						break;
					}	
					tmpCnt++;
				}	
		}	
			sparge_sir();
		afisare_buffer(buffer_rec);	
		Comanda_Display(0xD4);
		Data_Display(Buton_JS[0]);
		Data_Display(Buton_JD[0]);
		Data_Display(Buton_Stanga[0]);
		Data_Display(Buton_Dreapta[0]);
		Data_Display(Buton_Jos[0]);
		Data_Display(Buton_Sus[0]);		
		Data_Display(Buton_Mijloc[0]);		
}	

/***************************************************************************
**                                                                        **
**    functia pentru comanda unui led                                     **
 ***************************************************************************/
bit LED(char cmd[]){
	if(cmd[0] == '0')
			return(0);
	if(cmd[0] == '1')
		  return(1);
}	

void sparge_sir(void){
	char tmpSir[5];
	int  i;
	int  j;

//JS	
	strcpy(tmpSir,"\0");
	i=0;
	j=0;
	while(buffer_rec[i] != ','){
		tmpSir[j] = buffer_rec[i];
		i++;
		j++;
	}
	tmpSir[j]='\0';
	strcpy(Buton_JS,tmpSir);

	strcpy(tmpSir,"\0");
	j=0;
	i++;
	while(buffer_rec[i] != ','){
		tmpSir[j] = buffer_rec[i];
		i++;
		j++;
	}
	tmpSir[j]='\0';
	strcpy(JS_X,tmpSir);

	strcpy(tmpSir,"\0");
	j=0;
	i++;
	while(buffer_rec[i] != ','){
		tmpSir[j] = buffer_rec[i];
		i++;
		j++;
	}
	tmpSir[j]='\0';
	strcpy(JS_Y,tmpSir);

	//JD
	strcpy(tmpSir,"\0");
	i++;
	j=0;
	while(buffer_rec[i] != ','){
		tmpSir[j] = buffer_rec[i];
		i++;
		j++;
	}
	tmpSir[j]='\0';
	strcpy(Buton_JD,tmpSir);

	strcpy(tmpSir,"\0");
	j=0;
	i++;
	while(buffer_rec[i] != ','){
		tmpSir[j] = buffer_rec[i];
		i++;
		j++;
	}
	tmpSir[j]='\0';
	strcpy(JD_X,tmpSir);

	strcpy(tmpSir,"\0");
	j=0;
	i++;
	while(buffer_rec[i] != ','){
		tmpSir[j] = buffer_rec[i];
		i++;
		j++;
	}
	tmpSir[j]='\0';
	strcpy(JD_Y,tmpSir);
	
//Buton Stanga
	strcpy(tmpSir,"\0");
	j=0;
	i++;
	while(buffer_rec[i] != ','){
		tmpSir[j] = buffer_rec[i];
		i++;
		j++;
	}
	tmpSir[j]='\0';
	strcpy(Buton_Stanga,tmpSir);

//Buton Dreapta
	strcpy(tmpSir,"\0");
	j=0;
	i++;
	while(buffer_rec[i] != ','){
		tmpSir[j] = buffer_rec[i];
		i++;
		j++;
	}
	tmpSir[j]='\0';
	strcpy(Buton_Dreapta,tmpSir);
	
//Buton Jos
	strcpy(tmpSir,"\0");
	j=0;
	i++;
	while(buffer_rec[i] != ','){
		tmpSir[j] = buffer_rec[i];
		i++;
		j++;
	}
	tmpSir[j]='\0';
	strcpy(Buton_Sus,tmpSir);

//Buton Sus
	strcpy(tmpSir,"\0");
	j=0;
	i++;
	while(buffer_rec[i] != ','){
		tmpSir[j] = buffer_rec[i];
		i++;
		j++;
	}
	tmpSir[j]='\0';
	strcpy(Buton_Jos,tmpSir);

//Buton Stanga
	strcpy(tmpSir,"\0");
	j=0;
	i++;
	while(buffer_rec[i] != ','){
		tmpSir[j] = buffer_rec[i];
		i++;
		j++;
	}
	tmpSir[j]='\0';
	strcpy(Buton_Mijloc,tmpSir);
	
}	