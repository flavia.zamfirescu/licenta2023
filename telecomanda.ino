#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

#define Joystick_Stanga_X      A0
#define Joystick_Stanga_Y      A1
#define Joystick_Dreapta_X     A2
#define Joystick_Dreapta_Y     A3

#define Joystick_Stanga_B      7
#define Joystick_Dreapta_B     8
#define Buton_Stanga          9
#define Buton_Dreapta         10
#define Buton_Sus             11
#define Buton_Jos             12
#define Buton_Mijloc          13

char   rez_Butoane[10];
char   rez_JS[13];
char   rez_JD[13];
char   rez_B_Stanga[2];
char   rez_B_Dreapta[2];
char   rez_B_Sus[2];
char   rez_B_Jos[2];
char   rez_B_Mijloc[2];
char   sir_tr[100];


// Afisajul LCD la adresa 0x27 pentru 20 caractere si 4 linii
LiquidCrystal_I2C lcd(0x20, 20, 4);
void  afis_ecran(char sir_afis[80]);

//control joystick
void joystick(int j_X,int j_Y, int j_B, char *rez);
//control buton
void buton(int B, char *rez);
void r_B(char *rez);

void setup() {
  char tmpStart;

  pinMode(Joystick_Stanga_B,INPUT_PULLUP);
  pinMode(Joystick_Dreapta_B,INPUT_PULLUP);
  pinMode(Buton_Stanga,INPUT_PULLUP);
  pinMode(Buton_Dreapta,INPUT_PULLUP);
  pinMode(Buton_Sus,INPUT_PULLUP);
  pinMode(Buton_Jos,INPUT_PULLUP);
  pinMode(Buton_Mijloc,INPUT_PULLUP);

// initialize the LCD
	lcd.begin();
//            *       Linia 0    **     Linia 1      **   Linia 2        **       Linia 3    *  
//            01234567890123456789012345678901234567890123456789012345678901234567890123456789
  afis_ecran(" PROIECT DE DIPLOMA   ZAMFIRESCU FLAVIA   TELECOMANDA RADIO    ARDUINO UNO      ");
//  while(digitalRead(Buton_Mijloc)==HIGH);
//  delay(1000);
//  lcd.clear();
  Serial.begin(4800);
//asteapta caracterul S
  while(1){
    tmpStart = Serial.read();
     if(tmpStart == 'S'){
//                   *       Linia 0    **     Linia 1      **   Linia 2        **       Linia 3    *  
//                    01234567890123456789012345678901234567890123456789012345678901234567890123456789
          afis_ecran(" Conexiune stabilita                                                            ");
          break;
    }
  }  
  delay(1000);
  Serial.write('R');
  lcd.clear();
}

void loop() {
//verifica butonul Buton Stanga
  buton(Buton_Stanga, rez_B_Stanga);
//verifica butonul Buton Dreapta
  buton(Buton_Dreapta, rez_B_Dreapta);
//verifica butonul Buton Sus
  buton(Buton_Sus, rez_B_Sus);
//verifica butonul Buton Jos
  buton(Buton_Jos, rez_B_Jos);
//verifica butonul Buton Mijloc
  buton(Buton_Mijloc, rez_B_Mijloc);
  r_B(rez_Butoane);
//Joystick Stanga  
  joystick(Joystick_Stanga_X,Joystick_Stanga_Y, Joystick_Stanga_B,rez_JS);  
//Joystick Dreapta  
  joystick(Joystick_Dreapta_X,Joystick_Dreapta_Y, Joystick_Dreapta_B,rez_JD);  
//afisare date
//Joystick Stanga  
  lcd.setCursor(0,0);             
  lcd.print("Joystick B  X   Y");
  lcd.setCursor(0,1);
  lcd.print("Stanga:  ");
  lcd.print(rez_JS);
  lcd.print(" ");
//Joystick Dreapta  
  lcd.setCursor(0,2);
  lcd.print("Dreapta: ");
  lcd.print(rez_JD);
  lcd.print(" ");
//Butoane  
  lcd.noBlink();
  lcd.setCursor(0,3);
  lcd.print("Butoane: ");
  lcd.print(rez_Butoane);

//constructie sir de transmis
  strcpy(sir_tr,rez_JS);
  strcat(sir_tr,rez_JD);
  strcat(sir_tr,rez_Butoane);
  strcat(sir_tr,"\0");
  
  while(1){
    if(Serial.available()>0){
      char tRec = Serial.read();
      if( tRec == 'D')
        break;
    }
  }
  
  Serial.write(sir_tr,50);
//  delay(100);
}

//afisare sir pe LCD
void  afis_ecran(char sir_afis[80]){
int   col;
int   linie;
int   i;

  i=0;
  for(linie=0;linie<4;linie++){
    for(col=0;col<20;col++){
      lcd.setCursor(col,linie);
      lcd.print(sir_afis[i]);
      i++;
    }
  }
}

//control joystick
void joystick(int j_X,int j_Y, int j_B,char *rez){
  int tmpX;
  int tmpY;
  char tmpRezX[5];
  char tmpRezY[5];
  char tmpRezB[1];
  char tmpRez[13];

//citire X
  tmpX = analogRead(j_X);
  itoa(tmpX,tmpRezX,10);
//citire Y  
  tmpY = analogRead(j_Y);
  itoa(tmpY,tmpRezY,10);
//citire Buton
  if(digitalRead(j_B)==LOW){
     strcpy(tmpRezB,"1");
  }
  if(digitalRead(j_B)==HIGH){
     strcpy(tmpRezB,"0");
  }
 
  strcpy(tmpRez,tmpRezB);
  strcat(tmpRez,","); 
  strcat(tmpRez,tmpRezX);
  strcat(tmpRez,",");
  strcat(tmpRez,tmpRezY);
  strcat(tmpRez,";");
  strcat(tmpRez,"\0");
  strcpy(rez,"\0");
  strcpy(rez,tmpRez);
}

//control buton
void buton(int B, char *rez){
  char tmpRezB[1];

//citire Buton
  if(digitalRead(B)==LOW){
     strcpy(tmpRezB,"1");
  }
  if(digitalRead(B)==HIGH){
     strcpy(tmpRezB,"0");
  }
  strcpy(rez,tmpRezB);
}

void r_B(char *rez){
  char tmpRezB[10];

  strcpy(tmpRezB,rez_B_Stanga);
  strcat(tmpRezB,",");  
  strcat(tmpRezB,rez_B_Dreapta);
  strcat(tmpRezB,",");
  strcat(tmpRezB,rez_B_Sus);
  strcat(tmpRezB,",");
  strcat(tmpRezB,rez_B_Jos);
  strcat(tmpRezB,",");
  strcat(tmpRezB,rez_B_Mijloc);
  strcat(tmpRezB,";");
  strcpy(rez,tmpRezB);
}